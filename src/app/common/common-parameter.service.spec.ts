import { TestBed } from '@angular/core/testing';

import { CommonParameterService } from './common-parameter.service';

describe('CommonParameterService', () => {
  let service: CommonParameterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CommonParameterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
