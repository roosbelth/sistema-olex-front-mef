export class Response {
  exito: number;
  mensaje: string;
  data: Object;
  content: Object;
}
