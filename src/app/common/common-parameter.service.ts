import { Injectable } from '@angular/core';
import {HttApiService} from '../service/htt-api.service';
import {HostService} from '../service/host.service';

@Injectable({
  providedIn: 'root'
})
export class CommonParameterService {

  constructor(
    private httpApiService: HttApiService,
    private  hostApi: HostService
  ) { }

  listarTipoParametro(){
    return this.httpApiService.get(
      this.hostApi.APPLICATION_API_HOST,
      "/tipoParametro/listarTipoParametro"
    )
  }
}
