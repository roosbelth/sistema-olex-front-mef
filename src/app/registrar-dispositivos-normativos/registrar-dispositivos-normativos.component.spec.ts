import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrarDispositivosNormativosComponent } from './registrar-dispositivos-normativos.component';

describe('RegistrarDispositivosNormativosComponent', () => {
  let component: RegistrarDispositivosNormativosComponent;
  let fixture: ComponentFixture<RegistrarDispositivosNormativosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistrarDispositivosNormativosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrarDispositivosNormativosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
