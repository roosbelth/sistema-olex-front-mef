import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoParametroComponent } from './tipo-parametro.component';

describe('TipoParametroComponent', () => {
  let component: TipoParametroComponent;
  let fixture: ComponentFixture<TipoParametroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TipoParametroComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoParametroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
