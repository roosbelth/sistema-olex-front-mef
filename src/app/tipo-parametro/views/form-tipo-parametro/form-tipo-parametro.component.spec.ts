import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormTipoParametroComponent } from './form-tipo-parametro.component';

describe('FormTipoParametroComponent', () => {
  let component: FormTipoParametroComponent;
  let fixture: ComponentFixture<FormTipoParametroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormTipoParametroComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormTipoParametroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
