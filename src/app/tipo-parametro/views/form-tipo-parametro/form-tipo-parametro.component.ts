import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {tipoParametro} from '../../model/tipo-parametro';
import {TipoParametroService} from '../../services/tipo-parametro.service';
import {NotifierService} from 'angular-notifier';

@Component({
  selector: 'app-form-tipo-parametro',
  templateUrl: './form-tipo-parametro.component.html',
  styleUrls: ['./form-tipo-parametro.component.scss']
})
export class FormTipoParametroComponent implements OnInit {
  public tipoParametroForm = this.formBuilder.group({
    tipoParametro: ['',  [Validators.required, Validators.maxLength(150)]],
    descripcion: ['',  [Validators.required, Validators.maxLength(250)]]
  })
  local_data:any;
  public parametro:tipoParametro = new tipoParametro();

  //parametro = Object.assign({}, tipoParametro);

  private notifyTipoParametro;
  //public descripcion:string="";
  //public accion:string='';

  constructor(
    private tipoParametroService: TipoParametroService,
    public dialogRef: MatDialogRef<FormTipoParametroComponent>,
    private formBuilder: FormBuilder,
    notifierService: NotifierService,
    @Inject(MAT_DIALOG_DATA) public datos:any
  ) {
    console.log(datos);
    //this.accion = datos.accion;
    if (datos.accion=='EDITAR'){
      let datoooo = datos.data;
      this.parametro = datoooo;// datos.data;
    }
    this.local_data = {...this.local_data};
    this.notifyTipoParametro = notifierService;
  }



  ngOnInit(): void {
    //console.log('DATOOOOOOOOSSSS ACCCIONNNNNNNNNNNNNNNN', this.accion);
    this.parametro.usuarioRegistro = 'ROOSBELTH';
  }

  close(){
    this.dialogRef.close();
  }

  registrar() {
    this.parametro.usuarioRegistro = 'ROOSBELTH';
    this.tipoParametroService.guardar(this.parametro).subscribe((response: any) => {
      console.log(response);
      if (response.estado) {
        this.dialogRef.close(this.parametro);
      } else {
        this.notifyTipoParametro.notify('error', 'Error al registrar!');
      }
    });
  }

  editar(){
    console.log('DATOS PARA GUARDAR', this.parametro);
    this.tipoParametroService.actualizar(this.parametro).subscribe((response: any) => {
      console.log(response);
      if (response.estado) {
        this.dialogRef.close(this.parametro);
      } else {
        this.notifyTipoParametro.notify('error', 'Error al registrar!');
      }
    });
  }

}
