import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {VERSION} from '@angular/cdk';
import {TipoParametroService} from '../services/tipo-parametro.service';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatTable} from '@angular/material/table';
import {NotifierService} from 'angular-notifier';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material/dialog';
import {FormTipoParametroComponent} from './form-tipo-parametro/form-tipo-parametro.component';
import {tipoParametro} from '../model/tipo-parametro';

@Component({
  selector: 'app-tipo-parametro',
  templateUrl: './tipo-parametro.component.html',
  styleUrls: ['./tipo-parametro.component.scss']
})
export class TipoParametroComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator | undefined;
  @ViewChild(MatTable, {static: true}) table!: MatTable<any>;

  public lst: any[] = [];
  //public columnas: string[] = ['numeracion', 'actions', 'tipoParametro', 'descripcion', 'estado', 'fechaCreacion', 'usuarioCreacion', 'fechaModificacion', 'usuarioModificacion'];
  public columnas: string[] = ['numeracion', 'actions', 'tipoParametro', 'descripcion', 'estado', 'fechaCreacion', 'usuarioCreacion'];
  totalElements: number = 0;
  page: number= 0;
  size: number= 12;
  tipoParametro: tipoParametro;
  parametro: tipoParametro  = new tipoParametro();
  filtro:string='';
  version = VERSION;

  public loginForm = this.formBuilder.group({
    descripcion: ['',  Validators.required],
    tipoParametro: ['', Validators.required],
  });

  private notifier;
  constructor(
    private formBuilder: FormBuilder,
    private tipoParametroService: TipoParametroService,
    notifierService: NotifierService,
    private dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public datos:any
  ) {
    this.notifier = notifierService;
  }

  ngOnInit(): void {
    this.inicializar();
  }

  inicializar() {
    this.tipoParametroService.listarTipoParametro(this.page, this.size, 'des', this.filtro).subscribe((resp: any) => {
      this.lst = this.numeracion(resp.data.content);
      this.totalElements = resp.data.totalElements;
      console.log(this.lst);
    });
  }

  registrar(){
    const dialogRef = this.dialog.open(FormTipoParametroComponent, {
      width: "40vw",
      disableClose:true,
      hasBackdrop:true,
      data: {accion: 'REGISTRAR', data: this.tipoParametro}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result!=undefined){
        console.log('The dialog was closed');
        console.log(result);
        this.notifier.notify('success', 'Registro exitoso!');
        this.inicializar();
      }
    });
  }

  openEdit(data){
    const dialogRefEdit = this.dialog.open(FormTipoParametroComponent, {
      width: "40vw",
      disableClose:true,
      hasBackdrop:true,
      data: {
        accion: 'EDITAR',
        data: {idTipoParametro: data.idTipoParametro, tipoParametro: data.tipoParametro, descripcion: data.descripcion}
      }
    });
    dialogRefEdit.afterClosed().subscribe(result => {
      if (result!=undefined){
        this.notifier.notify('success', 'Actualización exitoso!');
        this.inicializar();
      }
    });
  }

  cambiarEstado(data){
    console.log(data)
    if (data.estado){
      this.parametro.estado = 0;
    }else {
      this.parametro.estado = 1;
    }
    this.parametro.idTipoParametro = data.idTipoParametro;
    this.parametro.usuarioModifica = 'ROOSBELTH';
    this.tipoParametroService.cambiarEstado(this.parametro).subscribe((response: any) => {
      console.log(response);
      if (response.estado) {
        this.inicializar();
        this.notifier.notify('success', 'Cambios guardados exitosamente!');
      } else {
        this.notifier.notify('error', 'Error al guardados cambios!');
      }
    });
  }


  private numeracion(midata: any | any[]) {
    for (let i = 0; i < midata.length; i++) {
      midata[i].numeracion = ((i + 1)+(this.page*this.size));
    }
    return midata;
  }

  nextPage(event: PageEvent) {
    this.page = event.pageIndex;
    this.size = event.pageSize;
    this.inicializar();
  }
}
