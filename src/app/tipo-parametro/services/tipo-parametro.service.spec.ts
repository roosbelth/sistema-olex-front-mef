import { TestBed } from '@angular/core/testing';

import { TipoParametroService } from './tipo-parametro.service';

describe('TipoParametroService', () => {
  let service: TipoParametroService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TipoParametroService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
