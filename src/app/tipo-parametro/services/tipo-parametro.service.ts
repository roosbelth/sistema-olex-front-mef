import { Injectable } from '@angular/core';
import {HttApiService} from '../../service/htt-api.service';
import {HostService} from '../../service/host.service';

@Injectable({
  providedIn: 'root'
})
export class TipoParametroService {
  constructor(
    private httpApiService: HttApiService,
    private  hostApi: HostService
  ) { }

  listarTipoParametro(page: number, size: number, sort: string, filtro: string): any{
    return this.httpApiService.get(
      this.hostApi.APPLICATION_API_HOST,
      "/tipoParametro/inicializar?page="+page+"&size="+size+"&sort=tipoParametro,desc&filtro="+filtro+""
    )
  }

  guardar(parametro): any {
    return this.httpApiService.post(
      this.hostApi.APPLICATION_API_HOST,
      '/tipoParametro', parametro
    );
  }

  actualizar(parametro): any {
    return this.httpApiService.post(
      this.hostApi.APPLICATION_API_HOST,
      '/tipoParametro/actualizar', parametro
    );
  }

  cambiarEstado(parametro): any {
    return this.httpApiService.post(
      this.hostApi.APPLICATION_API_HOST,
      '/tipoParametro/cambiarEstado', parametro
    );
  }
}
