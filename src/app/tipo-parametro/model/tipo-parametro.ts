export class tipoParametro{
  idTipoParametro: number;
  tipoParametro: string;
  descripcion: string;
  estado: number;
  usuarioRegistro: string;
  usuarioModifica: string;
}
