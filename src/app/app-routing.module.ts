import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {MainComponent} from './main/main.component';
import {TipoParametroComponent} from './tipo-parametro/views/tipo-parametro.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {LoginComponent} from './login/login.component';
import {ParametroComponent} from './parametro/views/parametro.component';
import {
  InformeOpinionLegalAbsolucionConsultasComponent
} from './informe-opinion-legal-absolucion-consultas/informe-opinion-legal-absolucion-consultas.component';
import {RegistrarInformeOpiAbsolConsultasComponent} from './registrar-informe-opi-absol-consultas/registrar-informe-opi-absol-consultas.component';

const routes: Routes = [
  {path: 'olex',
    component: MainComponent,
    children : [
      {path: '', component: DashboardComponent},
      {path: 'tipoParametro', component: TipoParametroComponent},
      {path: 'parametrosGeneral', component: ParametroComponent},
      {path: 'informes-opinion-legal-absolucion-consultas', component: InformeOpinionLegalAbsolucionConsultasComponent},
      {path: 'registro-informes-opinion-legal-absolucion-consultas', component: RegistrarInformeOpiAbsolConsultasComponent}
    ]
  },
  //{path: '', component: LoginComponent}
  {path: '', component: LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
