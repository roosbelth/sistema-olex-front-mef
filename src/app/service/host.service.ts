import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HostService {
  public APPLICATION_API_HOST: string = "";
  constructor() {
    switch (environment.name) {
      case "desarrollo":
        this.APPLICATION_API_HOST = "http://localhost:9090/olex/api";
        break;
      case "produccion":
        this.APPLICATION_API_HOST = "http://localhost:8080/api";
        break;

    }
  }
}
