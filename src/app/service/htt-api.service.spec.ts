import { TestBed } from '@angular/core/testing';

import { HttApiService } from './htt-api.service';

describe('HttApiService', () => {
  let service: HttApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HttApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
