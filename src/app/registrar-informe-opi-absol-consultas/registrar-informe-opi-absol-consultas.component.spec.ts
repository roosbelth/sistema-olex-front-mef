import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrarInformeOpiAbsolConsultasComponent } from './registrar-informe-opi-absol-consultas.component';

describe('InformeOpinionAbsolucionConsultasComponent', () => {
  let component: RegistrarInformeOpiAbsolConsultasComponent;
  let fixture: ComponentFixture<RegistrarInformeOpiAbsolConsultasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistrarInformeOpiAbsolConsultasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrarInformeOpiAbsolConsultasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
