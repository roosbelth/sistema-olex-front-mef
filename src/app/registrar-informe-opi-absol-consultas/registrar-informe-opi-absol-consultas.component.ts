import {Component, OnInit} from '@angular/core';
import {VERSION} from '@angular/cdk';
import {PageEvent} from '@angular/material/paginator';

@Component({
  selector: 'app-registrar-informe-opi-absol-consultas',
  templateUrl: './registrar-informe-opi-absol-consultas.component.html',
  styleUrls: ['./registrar-informe-opi-absol-consultas.component.scss']
})
export class RegistrarInformeOpiAbsolConsultasComponent implements OnInit {

  public lstTipoParametro: any[] = [];
  public lstParametro: any[] = [];
  //public columnas: string[] = ['numeracion', 'actions','numeroInforme', 'fechaRegistro', 'asunto', 'solicitante', 'destinatario', 'materia', 'estado', 'fechaCreacion', 'usuarioCreacion'];
  public columnas: string[] = ['numeracion', 'actions', 'numeroInforme', 'fechaRegistro', 'asunto', 'solicitante', 'destinatario', 'materia', 'estado'];

  totalElements: number = 0;
  parametro: string = '';
  tipoParametro: number = 0;
  page: number = 0;
  size: number = 12;
  version = VERSION;

  constructor() {
  }

  ngOnInit(): void {
  }

  inicializar() {
    /*this.parametroService.listarParametro(this.page, this.size,  this.tipoParametro, this.parametro).subscribe((resp: any) => {
      this.lstParametro = this.numeracion(resp.data.content);
      this.totalElements = resp.data.totalElements;
      //console.log(this.lst);
      console.log(resp);
    });*/
  }

  nextPage(event: PageEvent) {
    this.page = event.pageIndex;
    this.size = event.pageSize;
    this.inicializar();
  }

}
