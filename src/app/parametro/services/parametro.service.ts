import { Injectable } from '@angular/core';
import {HttApiService} from '../../service/htt-api.service';
import {HostService} from '../../service/host.service';

@Injectable({
  providedIn: 'root'
})
export class ParametroService {

  constructor(
    private httpApiService: HttApiService,
    private  hostApi: HostService
  ) { }


  listarParametro(page: number, size: number, filtro: number, parametro: string): any{
    return this.httpApiService.get(
      this.hostApi.APPLICATION_API_HOST,
      "/parametro?page="+page+"&size="+size+"&tipoParametro="+filtro+"&descripcion="+parametro+""
    )
  }

  guardar(parametro): any {
    return this.httpApiService.post(
      this.hostApi.APPLICATION_API_HOST,
      '/parametro', parametro
    );
  }

  actualizar(parametro): any {
    return this.httpApiService.post(
      this.hostApi.APPLICATION_API_HOST,
      '/parametro/actualizar', parametro
    );
  }

  cambiarEstado(parametro): any {
    return this.httpApiService.post(
      this.hostApi.APPLICATION_API_HOST,
      '/parametro/cambiarEstado', parametro
    );
  }

}
