import { Component, OnInit } from '@angular/core';
import {CommonParameterService} from '../../common/common-parameter.service';
import {ParametroService} from '../services/parametro.service';
import {PageEvent} from '@angular/material/paginator';
import {VERSION} from '@angular/cdk';
import {NotifierService} from 'angular-notifier';
import {MatDialog} from '@angular/material/dialog';
import {FormParametroComponent} from './form-parametro/form-parametro.component';
import {ParametroModel} from '../model/parametro.model';

@Component({
  selector: 'app-parametro',
  templateUrl: './parametro.component.html',
  styleUrls: ['./parametro.component.scss']
})
export class ParametroComponent implements OnInit {

  //filtroParametro:string;
  //parametroEstado:string;
  public columnas: string[] = ['numeracion', 'actions','parametro', 'sigla', 'tipoParametro', 'estado', 'fechaCreacion', 'usuarioCreacion'];

  public lstTipoParametro: any[] = [];
  public lstParametro: any[] = [];
  //public lst: any[] = [];
  totalElements: number = 0;
  parametro:string = '';
  tipoParametro: number = 0;
  page: number= 0;
  size: number= 12;
  version = VERSION;
  parametroModel: ParametroModel = new ParametroModel();

  constructor(
    private commonParameterService: CommonParameterService,
    private parametroService: ParametroService,
    public notifierService: NotifierService,
    private dialogParametro: MatDialog,
  ) {
  }

  ngOnInit(): void {
    this.listarTipoParametro();
    this.inicializar();
  }


  listarTipoParametro(){
    this.commonParameterService.listarTipoParametro().subscribe((resp: any) => {
      this.lstTipoParametro = resp.data;
      console.log(this.lstTipoParametro);
    });
  }

  inicializar() {
    this.parametroService.listarParametro(this.page, this.size,  this.tipoParametro, this.parametro).subscribe((resp: any) => {
      this.lstParametro = this.numeracion(resp.data.content);
      this.totalElements = resp.data.totalElements;
      //console.log(this.lst);
      console.log(resp);
    });
  }

  registrar(){
    const dialogRef = this.dialogParametro.open(FormParametroComponent, {
      width: "40vw",
      disableClose:true,
      hasBackdrop:true,
      data: {accion: 'REGISTRAR', data: this.lstTipoParametro }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result!=undefined){
        console.log('The dialog was closed');
        console.log(result);
        this.notifierService.notify('success', 'Registro exitoso!');
        this.inicializar();
      }
    });
  }

  openEdit(data){
    console.log(data);
    const dialogRefEdit = this.dialogParametro.open(FormParametroComponent, {
      width: "40vw",
      disableClose:true,
      hasBackdrop:true,
      data: {
        accion: 'EDITAR',
        data: {
          idParametro: data.idParametro, descripcion: data.descripcion, sigla: data.sigla,
          tipoParametro: data.tipoParametro        }
      }
    });
    dialogRefEdit.afterClosed().subscribe(result => {
      if (result!=undefined){
        this.notifierService.notify('success', 'Actualización exitoso!');
        this.inicializar();
      }
    });
  }

  cambiarEstado(data){
    console.log(data)
    if (data.estado){
      this.parametroModel.estado = 0;
    }else {
      this.parametroModel.estado = 1;
    }
    this.parametroModel.idParametro = data.idParametro;
    this.parametroModel.usuarioModifica = 'ROOSBELTH';

    this.parametroService.cambiarEstado(this.parametroModel).subscribe((response: any) => {
      console.log(response);
      if (response.estado) {
        this.inicializar();
        this.notifierService.notify('success', 'Cambios guardados exitosamente!');
      } else {
        this.notifierService.notify('error', 'Error al guardados cambios!');
      }
    });
  }

  onCgangeSelect(){
    console.log(this.parametro);
  }

  private numeracion(midata: any | any[]) {
    for (let i = 0; i < midata.length; i++) {
      midata[i].numeracion = ((i + 1)+(this.page*this.size));
    }
    return midata;
  }

  nextPage(event: PageEvent) {
    this.page = event.pageIndex;
    this.size = event.pageSize;
    this.inicializar();
  }

}
