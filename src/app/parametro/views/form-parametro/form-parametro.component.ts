import {Component, Inject, OnInit} from '@angular/core';
import {ParametroModel} from '../../model/parametro.model';
import {ParametroService} from '../../services/parametro.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {NotifierService} from 'angular-notifier';
import {FormBuilder, Validators} from '@angular/forms';
import {tipoParametro} from '../../../tipo-parametro/model/tipo-parametro';
import {CommonParameterService} from '../../../common/common-parameter.service';

@Component({
  selector: 'app-form-parametro',
  templateUrl: './form-parametro.component.html',
  styleUrls: ['./form-parametro.component.scss']
})
export class FormParametroComponent implements OnInit {
  // VALIDAR CON FORMULARIOS REACTIVOS
  public parametroForm = this.formBuilder.group({
    tipoParametro: ['',  [Validators.required, Validators.maxLength(150)]],
    sigla: ['',  [Validators.required, Validators.maxLength(250)]],
    idTipoParametro: ['',  [Validators.required, Validators.pattern("^[0-9]*$")]],
  })


  parametro: ParametroModel = new ParametroModel();

  lstTipoParametro: any;
  tipoParametro: number = 0;

  constructor(
    private parametroService: ParametroService,
    private commonParameterService: CommonParameterService,
    private dialogParametro: MatDialogRef<FormParametroComponent>,
    public notificationService: NotifierService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private formBuilder: FormBuilder,
  ) {
    console.log('holasss',data);

    if (data.accion=='EDITAR'){
      this.parametro = data.data;
    }

    console.log(this.lstTipoParametro);
  }

  ngOnInit(): void {
    this.listarTipoParametro();
    this.parametro.usuarioRegistro = 'ROOSBELTH';
  }

  listarTipoParametro(){
    this.commonParameterService.listarTipoParametro().subscribe((resp: any) => {
      this.lstTipoParametro = resp.data;
      console.log(this.lstTipoParametro);
    });
  }

  close(){
    this.dialogParametro.close();
  }

  registrar() {
    console.log(this.parametro);

    this.parametroService.guardar(this.parametro).subscribe((response: any) => {
      console.log(response);
      if (response.estado) {
        this.dialogParametro.close(this.parametro);
      } else {
        this.notificationService.notify('error', 'Error al registrar!');
      }
    });
  }

  editar(){
    console.log('DATOS PARA GUARDAR', this.parametro);
    this.parametroService.actualizar(this.parametro).subscribe((response: any) => {
      console.log(response);
      if (response.estado) {
        this.dialogParametro.close(this.parametro);
      } else {
        this.notificationService.notify('error', 'Error al registrar!');
      }
    });
  }

}
