import {tipoParametro} from '../../tipo-parametro/model/tipo-parametro';

export class ParametroModel{
  idParametro: number;
  tipoParametro: any = {};
  descripcion: String;
  sigla: String;
  estado: number;
  usuarioRegistro: String;
  usuarioModifica: String;
}


