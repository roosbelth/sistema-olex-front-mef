import {Component, OnInit, HostBinding, EventEmitter} from '@angular/core';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import {Observable} from 'rxjs';
import {map, shareReplay} from 'rxjs/operators';
import {Router} from '@angular/router';
import {MatSidenav} from '@angular/material/sidenav';
import {MatSnackBar} from '@angular/material/snack-bar';
import {NavItem} from './menu.model';


@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  propagar = new EventEmitter<string>();


  public loading: boolean;
  public isAuthenticated: boolean;
  public title: string;

  public isBypass: boolean;
  public mobile: boolean;
  public isMenuInitOpen: boolean;

  constructor(private breakpointObserver: BreakpointObserver,
              private router: Router,
              private _snackBar: MatSnackBar) {
  }

  private sidenav: MatSidenav;

  public isMenuOpen = true;
  public contentMargin = 240;

  get isHandset(): boolean {
    //return this.breakpointObserver.isMatched('(max-width: 720px)');
    return this.breakpointObserver.isMatched('(max-width: 960px)');
  }

  //isHandset$: Observable<boolean> = this.breakpointObserver.observe('(max-width: 720px)')
  isHandset$: Observable<boolean> = this.breakpointObserver.observe('(max-width: 960px)')
    .pipe(
      map(result => result.matches),
      shareReplay(1),
    );

  // *********************************************************************************************
  // * LIFE CYCLE EVENT FUNCTIONS
  // *********************************************************************************************

  ngOnInit() {
    this.isMenuOpen = true;  // Open side menu by default
    this.title = 'Material Layout Demo';
  }

  ngDoCheck() {
    if (this.isHandset) {
      this.isMenuOpen = false;
    } else {
      this.isMenuOpen = true;
    }
  }

  // *********************************************************************************************
  // * COMPONENT FUNCTIONS
  // *********************************************************************************************

  public openSnackBar(msg: string): void {
    this._snackBar.open(msg, 'X', {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
      panelClass: 'notif-error'
    });
  }

  public onSelectOption(option: any): void {
    const msg = `Chose option ${option}`;
    this.openSnackBar(msg);

    /* To route to another page from here */
    // this.router.navigate(['/home']);
  }


  clicked: boolean = false;

  Clicked() {
    this.clicked = true;
  }



  procesaPropagar(mensaje) {
    console.log(mensaje);
  }



  menu1: NavItem [] = [
    {
      displayName: 'Escritorio',
      iconName: 'desktop_windows',
      route: '/olex',
    },
    {
      displayName: 'Consultas',
      /*iconName: 'description',*/
      iconName: 'pageview',
      children: [
        {
          displayName: 'Informe Opinión Legal y Absolución de Consultas',
          /*iconName: 'how_to_reg',*/
          iconName: 'plagiarism',
          route: '/olex/registro-informes-opinion-legal-absolucion-consultas'
        },
        {
          displayName: 'Dispositivos Normativos OEFA',
          iconName: 'find_in_page',
          route: '/olex/tipoParametro'
        }
      ]
    },
    {
      displayName: 'Registro',
      /*iconName: 'description',*/
      iconName: 'add_to_queue',
      children: [
        {
          displayName: 'Informe Opinión Legal y Absolución de Consultas',
          iconName: 'post_add',
          route: '/olex/informes-opinion-legal-absolucion-consultas'
        },
        {
          displayName: 'Dispositivos Normativos OEFA',
          iconName: 'playlist_add',
          route: '/olex/tipoParametro'
        }
      ]
    },
    {
      displayName: 'Sessión',
      //iconName: 'group',
      iconName: 'apps',
      children: [
        {
          displayName: 'Tipo Párametro',
          iconName: 'tune',
          route: '/olex/tipoParametro'
        },
        {
          displayName: 'Párametros Generales',
          iconName: 'business_center',
          route: '/olex/parametrosGeneral'
        },
        {
          displayName: 'Cerrar Sesión ',
          iconName: 'logout',
          route: '/'
        }
      ]
    }
  ];
}

