import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-modal-form',
  templateUrl: './modal-form.component.html',
  styleUrls: ['./modal-form.component.scss']
})
export class ModalFormComponent implements OnInit {
  // VALIDAR CON FORMULARIOS REACTIVOS
  public parametroForm = this.formBuilder.group({
    tipoParametro: ['',  [Validators.required, Validators.maxLength(150)]],
    sigla: ['',  [Validators.required, Validators.maxLength(250)]],
    idTipoParametro: ['',  [Validators.required, Validators.pattern("^[0-9]*$")]],
  })

  parametro:any;
  data:any;

  constructor(
    private formBuilder: FormBuilder,

    private dialogParametro: MatDialogRef<ModalFormComponent>,
  ) { }

  ngOnInit(): void {
  }

  close(){
    this.dialogParametro.close();
  }

}
