import { Component, OnInit } from '@angular/core';
import {FormParametroComponent} from '../parametro/views/form-parametro/form-parametro.component';
import {VERSION} from '@angular/cdk';
import {PageEvent} from '@angular/material/paginator';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {ModalFormComponent} from './modal-form/modal-form.component';
import {NotifierService} from 'angular-notifier';


@Component({
  selector: 'app-informe-opinion-legal-absolucion-consultas',
  templateUrl: './informe-opinion-legal-absolucion-consultas.component.html',
  styleUrls: ['./informe-opinion-legal-absolucion-consultas.component.scss'],
  providers: [
  ],
})
export class InformeOpinionLegalAbsolucionConsultasComponent implements OnInit {

  //tipoParametro: string;
  //parametro: string;
  public lstTipoParametro: any[] = [];
  public lstParametro: any[] = [];
  //public columnas: string[] = ['numeracion', 'actions','numeroInforme', 'fechaRegistro', 'asunto', 'solicitante', 'destinatario', 'materia', 'estado', 'fechaCreacion', 'usuarioCreacion'];
  public columnas: string[] = ['numeracion', 'actions','numeroInforme', 'fechaRegistro', 'asunto', 'solicitante', 'destinatario', 'materia', 'estado'];

  totalElements: number = 0;
  parametro:string = '';
  tipoParametro: number = 0;
  page: number= 0;
  size: number= 12;
  version = VERSION;

  constructor(
    private dialogRegistrarInforme: MatDialog,
    public notifierService: NotifierService,
  ) { }

  ngOnInit(): void {
  }

  inicializar() {
    /*this.parametroService.listarParametro(this.page, this.size,  this.tipoParametro, this.parametro).subscribe((resp: any) => {
      this.lstParametro = this.numeracion(resp.data.content);
      this.totalElements = resp.data.totalElements;
      //console.log(this.lst);
      console.log(resp);
    });*/
  }

  registrar(){
    const dialogRef = this.dialogRegistrarInforme.open(ModalFormComponent, {
      width: "88vw",
      maxWidth: '100vw',
      disableClose:true,
      hasBackdrop:true,
      data: {accion: 'REGISTRAR', data: this.lstTipoParametro }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result!=undefined){
        console.log('The dialog was closed');
        console.log(result);
        this.notifierService.notify('success', 'Registro exitoso!');
        this.inicializar();
      }
    });
  }

  onCgangeSelect(){

  }

  openEdit(data){
    /*console.log(data);
    const dialogRefEdit = this.dialogParametro.open(FormParametroComponent, {
      width: "40vw",
      disableClose:true,
      hasBackdrop:true,
      data: {
        accion: 'EDITAR',
        data: {
          idParametro: data.idParametro, descripcion: data.descripcion, sigla: data.sigla,
          tipoParametro: data.tipoParametro        }
      }
    });
    dialogRefEdit.afterClosed().subscribe(result => {
      if (result!=undefined){
        this.notifierService.notify('success', 'Actualización exitoso!');
        this.inicializar();
      }
    });*/
  }

  cambiarEstado(data){
    /*console.log(data)
    if (data.estado){
      this.parametroModel.estado = 0;
    }else {
      this.parametroModel.estado = 1;
    }
    this.parametroModel.idParametro = data.idParametro;
    this.parametroModel.usuarioModifica = 'ROOSBELTH';

    this.parametroService.cambiarEstado(this.parametroModel).subscribe((response: any) => {
      console.log(response);
      if (response.estado) {
        this.inicializar();
        this.notifierService.notify('success', 'Cambios guardados exitosamente!');
      } else {
        this.notifierService.notify('error', 'Error al guardados cambios!');
      }
    });*/
  }

  nextPage(event: PageEvent) {
    this.page = event.pageIndex;
    this.size = event.pageSize;
    this.inicializar();
  }




}
