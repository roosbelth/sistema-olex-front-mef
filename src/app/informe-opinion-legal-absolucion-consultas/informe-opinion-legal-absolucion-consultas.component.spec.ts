import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InformeOpinionLegalAbsolucionConsultasComponent } from './informe-opinion-legal-absolucion-consultas.component';

describe('InformeOpinionLegalAbsolucionConsultasComponent', () => {
  let component: InformeOpinionLegalAbsolucionConsultasComponent;
  let fixture: ComponentFixture<InformeOpinionLegalAbsolucionConsultasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InformeOpinionLegalAbsolucionConsultasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InformeOpinionLegalAbsolucionConsultasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
