import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { MaterialModule } from './common/material.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './common/header/header.component';
import { FooterComponent } from './common/footer/footer.component';
import { MainComponent } from './main/main.component';
import { TipoParametroComponent } from './tipo-parametro/views/tipo-parametro.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {NotifierModule, NotifierOptions} from 'angular-notifier';
import { FormTipoParametroComponent } from './tipo-parametro/views/form-tipo-parametro/form-tipo-parametro.component';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import { ParametroComponent } from './parametro/views/parametro.component';
import { FormParametroComponent } from './parametro/views/form-parametro/form-parametro.component';
import { InformeOpinionLegalAbsolucionConsultasComponent } from './informe-opinion-legal-absolucion-consultas/informe-opinion-legal-absolucion-consultas.component';
import { RegistrarInformeOpiAbsolConsultasComponent } from './registrar-informe-opi-absol-consultas/registrar-informe-opi-absol-consultas.component';
/*import {MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';*/

import {
  MomentDateAdapter,
  MAT_MOMENT_DATE_ADAPTER_OPTIONS,
} from '@angular/material-moment-adapter';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
} from '@angular/material/core';
import { ModalFormComponent } from './informe-opinion-legal-absolucion-consultas/modal-form/modal-form.component';
import { ConsultaDispositivosNormativosComponent } from './consulta-dispositivos-normativos/consulta-dispositivos-normativos.component';
import { RegistrarDispositivosNormativosComponent } from './registrar-dispositivos-normativos/registrar-dispositivos-normativos.component';

const customNotifierOptions: NotifierOptions = {
  position: {
    horizontal: {
      position: 'right',
      distance: 12
    },
    vertical: {
      position: 'top',
      distance: 12,
      gap: 10
    }
  },
  theme: 'material',
  behaviour: {
    autoHide: 5000,
    onClick: 'hide',
    onMouseover: 'pauseAutoHide',
    showDismissButton: true,
    stacking: 4
  },
  animations: {
    enabled: true,
    show: {
      preset: 'slide',
      speed: 300,
      easing: 'ease'
    },
    hide: {
      preset: 'fade',
      speed: 300,
      easing: 'ease',
      offset: 50
    },
    shift: {
      speed: 300,
      easing: 'ease'
    },
    overlap: 150
  }
};

export const MY_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'DD/MM/YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    MainComponent,
    TipoParametroComponent,
    DashboardComponent,
    LoginComponent,
    FormTipoParametroComponent,
    ParametroComponent,
    FormParametroComponent,
    InformeOpinionLegalAbsolucionConsultasComponent,
    RegistrarInformeOpiAbsolConsultasComponent,
    ModalFormComponent,
    ConsultaDispositivosNormativosComponent,
    RegistrarDispositivosNormativosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NotifierModule.withConfig(customNotifierOptions)
  ],
  providers: [
    { provide: MAT_DIALOG_DATA, useValue: {} },
    { provide: MatDialogRef, useValue: {}, },
    {provide: MAT_DATE_LOCALE, useValue: 'es-ES'},
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
    },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
