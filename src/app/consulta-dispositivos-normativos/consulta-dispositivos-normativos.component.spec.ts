import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultaDispositivosNormativosComponent } from './consulta-dispositivos-normativos.component';

describe('ConsultaDispositivosNormativosComponent', () => {
  let component: ConsultaDispositivosNormativosComponent;
  let fixture: ComponentFixture<ConsultaDispositivosNormativosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConsultaDispositivosNormativosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultaDispositivosNormativosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
